//This is test suite
describe("Cheapest Operator Test Suite", function() {
    //This will be called before running each spec
    beforeEach(function() {

        cheapest = new Cheapest();

        operatorA = [
        {countryAreaCode:'1', price: 0.9},
        {countryAreaCode:'268', price:5.1},
        {countryAreaCode:'46', price:0.17},
        {countryAreaCode:'4620', price:0.0},
        {countryAreaCode:'468', price:0.15},
        {countryAreaCode:'4631', price:0.15},
        {countryAreaCode:'4673', price:0.9},
        {countryAreaCode:'46732', price:1.1}
        
        ];

        operatorB = [
        {countryAreaCode:'1', price: 0.92},
        {countryAreaCode:'44', price:0.5},
        {countryAreaCode:'46', price:0.2},
        {countryAreaCode:'467', price:1.0},
        {countryAreaCode:'48', price:1.2}
        ];
        
        operators = [operatorA,operatorB];

        telephoneNumber = '4673212345';

        value = {countryAreaCode:'46732', price:1.1};
        value2 = {countryAreaCode:'46732', price:0.1};
        value3 = {countryAreaCode:'4673', price:0.0};
    
    });
 
    

 
    it("Test Cheapest for One Operator", function() {
       
       expect(cheapest.oneOperator(operatorA,telephoneNumber)).toEqual({countryAreaCode:'46732', price:1.1});
    }); 

    it("Test Cheapest in a List of Operators", function() {
       
       expect(cheapest.manyOperators(operators,telephoneNumber,cheapest.oneOperator)).toEqual({countryAreaCode:'46732', price:1.1});
    }); 

    it("Test Compare Value same area code, result cheaper value", function() {
       
       expect(cheapest.compareValue(value,value2)).toEqual({countryAreaCode:'46732', price:0.1});
    }); 

    it("Test Compare Value different area code,result longer area code", function() {
       
       expect(cheapest.compareValue(value,value3)).toEqual({countryAreaCode:'46732', price:1.1});
    }); 
});