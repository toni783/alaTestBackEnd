function Cheapest() {
}

Cheapest.prototype.oneOperator = function(operator,telephoneNumber) {
    //variable declarations  
    var tel = telephoneNumber;
    var auxOperator = [];
    var cheapest={};
    
    //formatting for the telephone Number if needed 
    //tel = _.replace(tel, new RegExp(/\D/,"g"), '');
    
 
  _.forIn(operator, function(value, key) {
   
    if(tel.startsWith(value.countryAreaCode)){
      
      if(_.isEmpty(cheapest)){
          cheapest = value;
        }else{
          
           cheapest=Cheapest.prototype.compareValue(value,cheapest); 
        }
        
    }
  
  });
  

  return cheapest;
};

Cheapest.prototype.manyOperators = function(operators,telephoneNumber) {
  
  //variable declarations 
  var cheapest={};
  var valueAux={};
  
  _.forIn(operators, function(value, key) {
    
    
   if(_.isEmpty(cheapest)){
      cheapest = Cheapest.prototype.oneOperator(value,telephoneNumber);
    }else{
      
      valueAux = Cheapest.prototype.oneOperator(value,telephoneNumber);
      
       cheapest=Cheapest.prototype.compareValue(valueAux,cheapest);
    }
  
   
  });
  
  return cheapest;
};

Cheapest.prototype.compareValue = function(value,cheapest) {
      
       if(_.size(value.countryAreaCode)>_.size(cheapest.countryAreaCode)){
         return value;
       }else{
         
         if(_.size(cheapest.countryAreaCode)>_.size(value.countryAreaCode)){
           return cheapest;
         }else{
            if(value.price < cheapest.price ){
             return value;
           }else{
             return cheapest;
           }
         }
        
       } 

};



