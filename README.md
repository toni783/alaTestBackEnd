Hello Programming Exercise - alaTest(back-end)!
notes: 

Code for the solution of the test can be find in ~/jasmine/src/Cheapest.js 
I did  not use a database or create a GUI  as the instructions say 
Problem solved in Javascript with the library lodash
Unit test Done  with Jasmine
Start the unit test open the file that can be find in ~/jasmine/SpecRunner.html
Spec file can be find in ~/jasmine/spec/CheapestSpec.js
Made By : Gilbert Antonio Morett Rodrigues